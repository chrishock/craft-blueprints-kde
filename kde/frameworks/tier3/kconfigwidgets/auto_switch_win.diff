diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index e475aec..be0608e 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -23,6 +23,13 @@ target_sources(KF5ConfigWidgets PRIVATE
 
   kconfigwidgets.qrc
 )
+
+if (WIN32)
+  target_sources(KF5ConfigWidgets PRIVATE
+    windowsmessagesnotifier.cpp
+  )
+endif()
+
 ecm_qt_declare_logging_category(KF5ConfigWidgets
     HEADER kconfigwidgets_debug.h
     IDENTIFIER KCONFIG_WIDGETS_LOG
@@ -56,6 +63,11 @@ target_link_libraries(KF5ConfigWidgets
     KF5::GuiAddons # KColorScheme uses KColorUtils
     KF5::I18n # For action and widget texts
 )
+
+if (WIN32)
+  target_link_libraries(KF5ConfigWidgets PRIVATE advapi32)
+endif()
+
 if (TARGET KF5::Auth)
   target_link_libraries(KF5ConfigWidgets PUBLIC KF5::Auth) # KCModule uses KAuth::Action TODO KF6 Change to KF5::AuthCore
 else()
diff --git a/src/kcolorschememanager.cpp b/src/kcolorschememanager.cpp
index 4ceb2ab..6b66d18 100644
--- a/src/kcolorschememanager.cpp
+++ b/src/kcolorschememanager.cpp
@@ -24,15 +24,24 @@
 #include <QStyle>
 
 constexpr int defaultSchemeRow = 0;
+static bool s_overrideAutoSwitch = false;
+static QString autoColorSchemePath;
+#ifdef Q_OS_WIN
+WindowsMessagesNotifier KColorSchemeManagerPrivate::m_windowsMessagesNotifier = WindowsMessagesNotifier();
+#endif
 
-static void activateScheme(const QString &colorSchemePath)
+static void activateScheme(const QString &colorSchemePath, bool overrideAutoSwitch = true)
 {
+    s_overrideAutoSwitch = overrideAutoSwitch;
     // hint for plasma-integration to synchronize the color scheme with the window manager/compositor
     // The property needs to be set before the palette change because is is checked upon the
     // ApplicationPaletteChange event.
     qApp->setProperty("KDE_COLOR_SCHEME_PATH", colorSchemePath);
     if (colorSchemePath.isEmpty()) {
         qApp->setPalette(KColorScheme::createApplicationPalette(KSharedConfig::Ptr(nullptr)));
+        // enable auto-switch when Default color scheme is set
+        s_overrideAutoSwitch = false;
+        qApp->setPalette(KColorScheme::createApplicationPalette(KSharedConfig::openConfig(autoColorSchemePath)));
     } else {
         qApp->setPalette(KColorScheme::createApplicationPalette(KSharedConfig::openConfig(colorSchemePath)));
     }
@@ -72,12 +81,25 @@ QIcon KColorSchemeManagerPrivate::createPreview(const QString &path)
 KColorSchemeManagerPrivate::KColorSchemeManagerPrivate()
     : model(new KColorSchemeModel())
 {
+#ifdef Q_OS_WIN
+    QAbstractEventDispatcher::instance()->installNativeEventFilter(&m_windowsMessagesNotifier);
+#endif
 }
 
 KColorSchemeManager::KColorSchemeManager(QObject *parent)
     : QObject(parent)
     , d(new KColorSchemeManagerPrivate)
 {
+#ifdef Q_OS_WIN
+    connect(&d->getWindowsMessagesNotifier(), &WindowsMessagesNotifier::wm_colorSchemeChanged, this, [this](){
+        const QString colorSchemeToApply = d->getWindowsMessagesNotifier().preferDarkMode() ? d->getDarkColorScheme() : d->getLightColorScheme();
+        autoColorSchemePath = this->indexForScheme(colorSchemeToApply).data(Qt::UserRole).toString();
+        if (!s_overrideAutoSwitch) {
+            ::activateScheme(this->indexForScheme(colorSchemeToApply).data(Qt::UserRole).toString(), false);
+        }
+    });
+    d->getWindowsMessagesNotifier().handleWMSettingChange();
+#endif
 }
 
 KColorSchemeManager::~KColorSchemeManager()
diff --git a/src/kcolorschememanager_p.h b/src/kcolorschememanager_p.h
index 88cd016..f7c73e3 100644
--- a/src/kcolorschememanager_p.h
+++ b/src/kcolorschememanager_p.h
@@ -11,6 +11,9 @@
 #include <memory>
 
 #include "kcolorschememodel.h"
+#ifdef Q_OS_WIN
+#include "windowsmessagesnotifier.h"
+#endif
 
 class KColorSchemeManagerPrivate
 {
@@ -20,6 +23,16 @@ public:
     std::unique_ptr<KColorSchemeModel> model;
 
     static QIcon createPreview(const QString &path);
+
+#ifdef Q_OS_WIN
+    static WindowsMessagesNotifier m_windowsMessagesNotifier;
+    WindowsMessagesNotifier &getWindowsMessagesNotifier() {return m_windowsMessagesNotifier;}
+    const QString& getLightColorScheme() {return m_lightColorScheme;}
+    const QString& getDarkColorScheme() {return m_darkColorScheme;}
+
+private:
+    QString m_lightColorScheme = QStringLiteral("Breeze"), m_darkColorScheme = QStringLiteral("Breeze Dark");
+#endif
 };
 
 #endif
diff --git a/src/windowsmessagesnotifier.cpp b/src/windowsmessagesnotifier.cpp
new file mode 100644
index 0000000..5673a73
--- /dev/null
+++ b/src/windowsmessagesnotifier.cpp
@@ -0,0 +1,41 @@
+/*
+ * SPDX-FileCopyrightText: 2020 Piyush Aggarwal <piyushaggarwal002@gmail.com>
+ *
+ * SPDX-License-Identifier: LGPL-2.0-or-later
+ */
+
+#include "windowsmessagesnotifier.h"
+#include <QDebug>
+
+WindowsMessagesNotifier::WindowsMessagesNotifier() {}
+
+WindowsMessagesNotifier::~WindowsMessagesNotifier() {}
+
+bool WindowsMessagesNotifier::nativeEventFilter(const QByteArray &eventType, void *message, long *)
+{
+    MSG *msg = static_cast< MSG * >( message );
+    switch (msg->message)
+    {
+        case WM_SETTINGCHANGE: {
+            WindowsMessagesNotifier::handleWMSettingChange();
+            break;
+        }
+        default: {}
+    }
+    return false;
+}
+
+void WindowsMessagesNotifier::handleWMSettingChange()
+{
+    m_settings.sync();
+    const bool preferDarkModeNow = !(m_settings.value(QStringLiteral("SystemUsesLightTheme")).value<bool>());
+    if (m_preferDarkMode != preferDarkModeNow) {
+        m_preferDarkMode = preferDarkModeNow;
+        Q_EMIT WindowsMessagesNotifier::wm_colorSchemeChanged();
+    }
+}
+
+bool WindowsMessagesNotifier::preferDarkMode()
+{
+    return m_preferDarkMode;
+}
\ No newline at end of file
diff --git a/src/windowsmessagesnotifier.h b/src/windowsmessagesnotifier.h
new file mode 100644
index 0000000..120831b
--- /dev/null
+++ b/src/windowsmessagesnotifier.h
@@ -0,0 +1,38 @@
+/*
+ * SPDX-FileCopyrightText: 2020 Piyush Aggarwal <piyushaggarwal002@gmail.com>
+ *
+ * SPDX-License-Identifier: LGPL-2.0-or-later
+ */
+
+#ifndef WindowsMessagesNotifier_H
+#define WindowsMessagesNotifier_H
+
+#include <QSettings>
+#include <QAbstractNativeEventFilter>
+#include <QAbstractEventDispatcher>
+#include <QDebug>
+
+#include <windows.h>
+
+class WindowsMessagesNotifier
+    : public QObject, public QAbstractNativeEventFilter
+{
+    Q_OBJECT
+
+public:
+    WindowsMessagesNotifier();
+    ~WindowsMessagesNotifier();
+    virtual bool nativeEventFilter(const QByteArray &eventType, void *message, long *) Q_DECL_OVERRIDE;
+    void handleWMSettingChange();
+    bool preferDarkMode();
+
+Q_SIGNALS:
+    void wm_colorSchemeChanged();
+
+private:
+    const QString m_subKey {QStringLiteral("Software\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize")};
+    QSettings m_settings {QStringLiteral("HKEY_CURRENT_USER\\") + m_subKey, QSettings::NativeFormat};
+    bool m_preferDarkMode = false;
+};
+
+#endif // WindowsMessagesNotifier_H
